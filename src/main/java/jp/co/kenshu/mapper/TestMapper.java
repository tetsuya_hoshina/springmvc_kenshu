package jp.co.kenshu.mapper;

import java.util.List;

import jp.co.kenshu.dto.test.TestDto;
import jp.co.kenshu.entity.Test;

public interface TestMapper {
    Test getTest(int id);

    List<Test> getTestAll();

    // whereにオブジェクトを渡す
    Test getTestByDto(TestDto dto);

    // Insertサンプル
    int insertTest(String name);

    // Deleteサンプル
    int deleteTest(int id);

    // updateサンプル
    int updateTest(TestDto dto);

    // トランザクションテスト用の失敗メソッド
    int insertFailTest(Object object);
}

